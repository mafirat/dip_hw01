﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AguDIP
{
  public partial class Form1 : Form
  {
    public Bitmap Source { get; set; }
    public Bitmap Output { get; set; }
    private double[,] transition = new double[3, 3];
    public Form1()
    {
      InitializeComponent();
    }

    private void btn_selectImage_Click(object sender, EventArgs e)
    {
      FileDialog.DefaultExt = ".jpg";
      FileDialog.Filter = "Image Files(*.BMP;*.JPG;*.GIF)|*.BMP;*JPG;*.GIF|All files(*.*)|*.*";
      DialogResult dialogResult = FileDialog.ShowDialog();

      var path = FileDialog.FileName;
      Source = Image.FromFile(path) as Bitmap;
      PictureBox.Image = Source;
    }

    public Bitmap GetNegative()
    {
      Color read;
      int R = 0, G = 0, B = 0;

      int Height = Source.Height, Width = Source.Width;

      Output = new Bitmap(Width, Height);
      for (int x = 0; x < Width; x++)
      {
        for (int y = 0; y < Height; y++)
        {
          read = Source.GetPixel(x, y);
          R = 255 - read.R;
          G = 255 - read.G;
          B = 255 - read.B;
          Output.SetPixel(x, y, Color.FromArgb(R, G, B));
        }
      }
      return Output;
    }

    private void btn_negative_Click(object sender, EventArgs e)
    {
      PictureBox.Image = GetNegative();
      //Kucultme_DegistirmeMetodu();
    }

    private void bt_translate_Click(object sender, EventArgs e)
    {
      GetTransition();
      if (tb_translate_x.Text != "" && tb_translate_y.Text != "")
      {
        Color read;
        int translate_x = Convert.ToInt16(tb_translate_x.Text), translate_y = Convert.ToInt16(tb_translate_y.Text);
        int width = Source.Width, height = Source.Height;
        double newx = 0, newy = 0;
        Output = new Bitmap(width, height);
        for (int x = 0; x < width; x++)
        {
          for (int y = 0; y < height; y++)
          {
            read = Source.GetPixel(x, y);
            newx = x + translate_x;
            newy = y + translate_y;
            if ((newx > 0 && newx < width) && (newy > 0 && newy < height))
            {
              Output.SetPixel((int)newx, (int)newy, read);
            }
          }
        }
        PictureBox.Image = Output;
      }
    }

    private void btn_rotate_Click(object sender, EventArgs e)
    {
      if (rb_none.Checked)
      {
        Rotate();
      }
      else if (rb_blinear.Checked)
      {
        InterpolationRotate();
      }
      PictureBox.Image = Output;
    }
    public Bitmap Rotate()
    {
      Color read;
      int width = Source.Width;
      int height = Source.Height;
      Output = new Bitmap(width, height);

      double Angle = Convert.ToDouble(tb_rotate_angle.Text) * 2 * Math.PI / 360;
      double x2 = 0, y2 = 0;
      //Find center of image
      int x0 = width / 2;
      int y0 = height / 2;
      for (int x1 = 0; x1 < (width); x1++)
      {
        for (int y1 = 0; y1 < (height); y1++)
        {
          read = Source.GetPixel(x1, y1);
          //Rotateing
          x2 = Math.Cos(Angle) * (x1 - x0) - Math.Sin(Angle) * (y1 - y0) + x0;
          y2 = Math.Sin(Angle) * (x1 - x0) + Math.Cos(Angle) * (y1 - y0) + y0;

          if (x2 > 0 && x2 < width && y2 > 0 && y2 < height)
            Output.SetPixel((int)x2, (int)y2, read);
        }
      }
      return Output;
    }
    public void InterpolationRotate()
    {
      int width = Source.Width, height = Source.Height;
      Output = new Bitmap(width + 1, height + 1);
      int x, y;
      Color C1, C2, C3, C4;

      double w, h;

      double Angle = Convert.ToDouble(tb_rotate_angle.Text) * 2 * Math.PI / 360;
      double x2 = 0, y2 = 0;
      //Find center of image
      int x0 = width / 2;
      int y0 = height / 2;

      for (int i = 0; i < width - 1; i++)
      {
        for (int j = 0; j < height - 1; j++)
        {
          x2 = Math.Cos(Angle) * (i - x0) - Math.Sin(Angle) * (j - y0) + x0;
          y2 = Math.Sin(Angle) * (i - x0) + Math.Cos(Angle) * (j - y0) + y0;

          if (x2 > 0 && x2 < width && y2 > 0 && y2 < height)
          {
            x = (int)(x2);
            y = (int)(y2);
            w = (x2) - x;
            h = (y2) - y;
            C1 = Source.GetPixel(x, y);
            C2 = Source.GetPixel(x + 1, y);
            C3 = Source.GetPixel(x, y + 1);
            C4 = Source.GetPixel(x + 1, y + 1);
            int r = (int)(C1.R * (1 - w) * (1 - h) + C2.R * (w) * (1 - h) + C3.R * (h) * (1 - w) + C4.R * (w * h));
            int g = (int)(C1.G * (1 - w) * (1 - h) + C2.G * (w) * (1 - h) + C3.G * (h) * (1 - w) + C4.G * (w * h));
            int b = (int)(C1.B * (1 - w) * (1 - h) + C2.B * (w) * (1 - h) + C3.B * (h) * (1 - w) + C4.B * (w * h));
            Color col = Color.FromArgb(r, g, b);
            Output.SetPixel((int)x2, (int)y2, col);
          }
        }
      }
    }

    public void InterpolationScale()
    {
      double scale_x = Convert.ToDouble(tb_scale_x.Text), scale_y = Convert.ToDouble(tb_scale_y.Text);
      int nWidth = (int)(scale_x * Source.Width), nHeight = (int)(scale_y * Source.Height);
      Output = new Bitmap(nWidth, nHeight);
      int x, y;
      Color C1, C2, C3, C4;
      float x_ratio = ((float)(Source.Width - 1)) / nWidth;
      float y_ratio = ((float)(Source.Height - 1)) / nHeight;
      float w, h;

      for (int i = 0; i < nWidth; i++)
      {
        for (int j = 0; j < nHeight; j++)
        {
          x = (int)(x_ratio * i);
          y = (int)(y_ratio * j);
          w = (x_ratio * i) - x;
          h = (y_ratio * j) - y;

          C1 = Source.GetPixel(x, y);
          C2 = Source.GetPixel(x + 1, y);
          C3 = Source.GetPixel(x, y + 1);
          C4 = Source.GetPixel(x + 1, y + 1);

          int r = (int)((C1.R * (1 - w) * (1 - h)) + (C2.R * (w) * (1 - h)) + (C3.R * (h) * (1 - w)) + (C4.R * (w * h)));
          int g = (int)((C1.G * (1 - w) * (1 - h)) + (C2.G * (w) * (1 - h)) + (C3.G * (h) * (1 - w)) + (C4.G * (w * h)));
          int b = (int)((C1.B * (1 - w) * (1 - h)) + (C2.B * (w) * (1 - h)) + (C3.B * (h) * (1 - w)) + (C4.B * (w * h)));

          Color col = Color.FromArgb(r, g, b);

          Output.SetPixel(i, j, col);
        }
      }
    }

    public void Scale()
    {
      if (tb_scale_x.Text != "" && tb_scale_y.Text != "")
      {
        double scale_x = Convert.ToDouble(tb_scale_x.Text), scale_y = Convert.ToDouble(tb_scale_y.Text);
        Color OkunanRenk;
        int width = Source.Width;
        int height = Source.Height;
        Output = new Bitmap((int)(Source.Width * scale_x), (int)(Source.Height * scale_y));
        for (int x1 = 0; x1 < width; x1++)
        {
          for (int y1 = 0; y1 < height; y1++)
          {
            OkunanRenk = Source.GetPixel(x1, y1);
            Output.SetPixel((int)(x1 * scale_x), (int)(y1 * scale_y), OkunanRenk);
          }
        }
        PictureBox.Image = Output;
      }
      else
      {
        MessageBox.Show("Scale values cannot be null!", "Scale Error");
      }
    }

    private void GetTransition()
    {
      transition[0, 0] = Convert.ToDouble(tm11.Text);
      transition[0, 1] = Convert.ToDouble(tm12.Text);
      transition[0, 2] = Convert.ToDouble(tm12.Text);
      //                         
      transition[1, 0] = Convert.ToDouble(tm21.Text);
      transition[1, 1] = Convert.ToDouble(tm22.Text);
      transition[1, 2] = Convert.ToDouble(tm23.Text);
      //                         
      transition[2, 0] = Convert.ToDouble(tm31.Text);
      transition[2, 1] = Convert.ToDouble(tm32.Text);
      transition[2, 2] = Convert.ToDouble(tm33.Text);
    }

    private double[,] Multiply(double[,] point)
    {
      GetTransition();
      var m = new double[point.GetLength(0), transition.GetLength(1)];

      for (int i = 0; i < m.GetLength(0); i++)
      {
        for (int j = 0; j < m.GetLength(1); j++)
        {
          for (int k = 0; k < point.GetLength(1); k++)
          {
            m[i, j] += point[i, k] * transition[k, j];
          }
        }
      }
      return m;
    }

    private void btn_scale_Click(object sender, EventArgs e)
    {
      if (rb_none.Checked)
      {
        Scale();
      }
      else if (rb_blinear.Checked)
      {
        InterpolationScale();
      }
      PictureBox.Image = Output;
    }

    private void bt_apply_Click(object sender, EventArgs e)
    {
      GetTransition();
      double scale_x = transition[0, 0], scale_y = transition[1, 1];
      Color color;
      int width = (int)(Source.Width * scale_x);
      int height = (int)(Source.Height * scale_y);
      Output = new Bitmap(width, height);
      for (int x1 = 0; x1 < Source.Width; x1++)
      {
        for (int y1 = 0; y1 < Source.Height; y1++)
        {
          color = Source.GetPixel(x1, y1);
          var nPoint = Multiply(new double[1, 3] { { x1, y1, 1 } });
          if (((int)(nPoint[0, 0]) > 0 && (int)(nPoint[0, 0]) < width) && ((int)(nPoint[0, 1]) > 0 && (int)(nPoint[0, 1]) < height))
          {
            Output.SetPixel((int)(nPoint[0, 0]), (int)(nPoint[0, 1]), color);
          }
          
        }
      }
      PictureBox.Image = Output;
    }
  }
}
