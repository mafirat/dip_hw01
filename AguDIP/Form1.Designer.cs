﻿namespace AguDIP
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.btn_selectImage = new System.Windows.Forms.Button();
      this.FileDialog = new System.Windows.Forms.OpenFileDialog();
      this.PictureBox = new System.Windows.Forms.PictureBox();
      this.btn_negative = new System.Windows.Forms.Button();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      this.tm33 = new System.Windows.Forms.TextBox();
      this.tm32 = new System.Windows.Forms.TextBox();
      this.tm31 = new System.Windows.Forms.TextBox();
      this.tm23 = new System.Windows.Forms.TextBox();
      this.tm22 = new System.Windows.Forms.TextBox();
      this.tm21 = new System.Windows.Forms.TextBox();
      this.tm13 = new System.Windows.Forms.TextBox();
      this.tm12 = new System.Windows.Forms.TextBox();
      this.tm11 = new System.Windows.Forms.TextBox();
      this.bt_translate = new System.Windows.Forms.Button();
      this.btn_rotate = new System.Windows.Forms.Button();
      this.gb_translate = new System.Windows.Forms.GroupBox();
      this.tb_translate_y = new System.Windows.Forms.TextBox();
      this.tb_translate_x = new System.Windows.Forms.TextBox();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.tb_rotate_angle = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.gb_scale = new System.Windows.Forms.GroupBox();
      this.tb_scale_y = new System.Windows.Forms.TextBox();
      this.tb_scale_x = new System.Windows.Forms.TextBox();
      this.btn_scale = new System.Windows.Forms.Button();
      this.gb_interpolation = new System.Windows.Forms.GroupBox();
      this.rb_none = new System.Windows.Forms.RadioButton();
      this.rb_blinear = new System.Windows.Forms.RadioButton();
      this.bt_apply = new System.Windows.Forms.Button();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).BeginInit();
      this.tableLayoutPanel1.SuspendLayout();
      this.gb_translate.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.gb_scale.SuspendLayout();
      this.gb_interpolation.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.SuspendLayout();
      // 
      // btn_selectImage
      // 
      this.btn_selectImage.Location = new System.Drawing.Point(13, 13);
      this.btn_selectImage.Name = "btn_selectImage";
      this.btn_selectImage.Size = new System.Drawing.Size(75, 23);
      this.btn_selectImage.TabIndex = 0;
      this.btn_selectImage.Text = "Select";
      this.btn_selectImage.UseVisualStyleBackColor = true;
      this.btn_selectImage.Click += new System.EventHandler(this.btn_selectImage_Click);
      // 
      // PictureBox
      // 
      this.PictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.PictureBox.Location = new System.Drawing.Point(198, 13);
      this.PictureBox.Name = "PictureBox";
      this.PictureBox.Size = new System.Drawing.Size(598, 373);
      this.PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.PictureBox.TabIndex = 1;
      this.PictureBox.TabStop = false;
      // 
      // btn_negative
      // 
      this.btn_negative.Location = new System.Drawing.Point(94, 13);
      this.btn_negative.Name = "btn_negative";
      this.btn_negative.Size = new System.Drawing.Size(75, 23);
      this.btn_negative.TabIndex = 2;
      this.btn_negative.Text = "Negative";
      this.btn_negative.UseVisualStyleBackColor = true;
      this.btn_negative.Click += new System.EventHandler(this.btn_negative_Click);
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 3;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.97959F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.02041F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 55F));
      this.tableLayoutPanel1.Controls.Add(this.tm33, 2, 2);
      this.tableLayoutPanel1.Controls.Add(this.tm32, 1, 2);
      this.tableLayoutPanel1.Controls.Add(this.tm31, 0, 2);
      this.tableLayoutPanel1.Controls.Add(this.tm23, 2, 1);
      this.tableLayoutPanel1.Controls.Add(this.tm22, 1, 1);
      this.tableLayoutPanel1.Controls.Add(this.tm21, 0, 1);
      this.tableLayoutPanel1.Controls.Add(this.tm13, 2, 0);
      this.tableLayoutPanel1.Controls.Add(this.tm12, 1, 0);
      this.tableLayoutPanel1.Controls.Add(this.tm11, 0, 0);
      this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 19);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 3;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(141, 100);
      this.tableLayoutPanel1.TabIndex = 3;
      // 
      // tm33
      // 
      this.tm33.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tm33.Location = new System.Drawing.Point(88, 71);
      this.tm33.Name = "tm33";
      this.tm33.Size = new System.Drawing.Size(50, 20);
      this.tm33.TabIndex = 8;
      this.tm33.Text = "1";
      this.tm33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // tm32
      // 
      this.tm32.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tm32.Location = new System.Drawing.Point(45, 71);
      this.tm32.Name = "tm32";
      this.tm32.Size = new System.Drawing.Size(37, 20);
      this.tm32.TabIndex = 7;
      this.tm32.Text = "0";
      this.tm32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // tm31
      // 
      this.tm31.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tm31.Location = new System.Drawing.Point(3, 71);
      this.tm31.Name = "tm31";
      this.tm31.Size = new System.Drawing.Size(36, 20);
      this.tm31.TabIndex = 6;
      this.tm31.Text = "0";
      this.tm31.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // tm23
      // 
      this.tm23.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tm23.Location = new System.Drawing.Point(88, 37);
      this.tm23.Name = "tm23";
      this.tm23.Size = new System.Drawing.Size(50, 20);
      this.tm23.TabIndex = 5;
      this.tm23.Text = "0";
      this.tm23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // tm22
      // 
      this.tm22.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tm22.Location = new System.Drawing.Point(45, 37);
      this.tm22.Name = "tm22";
      this.tm22.Size = new System.Drawing.Size(37, 20);
      this.tm22.TabIndex = 4;
      this.tm22.Text = "1";
      this.tm22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // tm21
      // 
      this.tm21.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tm21.Location = new System.Drawing.Point(3, 37);
      this.tm21.Name = "tm21";
      this.tm21.Size = new System.Drawing.Size(36, 20);
      this.tm21.TabIndex = 3;
      this.tm21.Text = "0";
      this.tm21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // tm13
      // 
      this.tm13.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tm13.Location = new System.Drawing.Point(88, 3);
      this.tm13.Name = "tm13";
      this.tm13.Size = new System.Drawing.Size(50, 20);
      this.tm13.TabIndex = 2;
      this.tm13.Text = "0";
      this.tm13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // tm12
      // 
      this.tm12.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tm12.Location = new System.Drawing.Point(45, 3);
      this.tm12.Name = "tm12";
      this.tm12.Size = new System.Drawing.Size(37, 20);
      this.tm12.TabIndex = 1;
      this.tm12.Text = "0";
      this.tm12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // tm11
      // 
      this.tm11.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tm11.Location = new System.Drawing.Point(3, 3);
      this.tm11.Name = "tm11";
      this.tm11.Size = new System.Drawing.Size(36, 20);
      this.tm11.TabIndex = 0;
      this.tm11.Text = "1";
      this.tm11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // bt_translate
      // 
      this.bt_translate.Location = new System.Drawing.Point(7, 46);
      this.bt_translate.Name = "bt_translate";
      this.bt_translate.Size = new System.Drawing.Size(140, 23);
      this.bt_translate.TabIndex = 4;
      this.bt_translate.Text = "Translate";
      this.bt_translate.UseVisualStyleBackColor = true;
      this.bt_translate.Click += new System.EventHandler(this.bt_translate_Click);
      // 
      // btn_rotate
      // 
      this.btn_rotate.Location = new System.Drawing.Point(7, 45);
      this.btn_rotate.Name = "btn_rotate";
      this.btn_rotate.Size = new System.Drawing.Size(140, 23);
      this.btn_rotate.TabIndex = 5;
      this.btn_rotate.Text = "Rotate";
      this.btn_rotate.UseVisualStyleBackColor = true;
      this.btn_rotate.Click += new System.EventHandler(this.btn_rotate_Click);
      // 
      // gb_translate
      // 
      this.gb_translate.Controls.Add(this.tb_translate_y);
      this.gb_translate.Controls.Add(this.tb_translate_x);
      this.gb_translate.Controls.Add(this.bt_translate);
      this.gb_translate.Location = new System.Drawing.Point(16, 43);
      this.gb_translate.Name = "gb_translate";
      this.gb_translate.Size = new System.Drawing.Size(153, 78);
      this.gb_translate.TabIndex = 6;
      this.gb_translate.TabStop = false;
      this.gb_translate.Text = "Translate";
      // 
      // tb_translate_y
      // 
      this.tb_translate_y.Location = new System.Drawing.Point(78, 20);
      this.tb_translate_y.Name = "tb_translate_y";
      this.tb_translate_y.Size = new System.Drawing.Size(69, 20);
      this.tb_translate_y.TabIndex = 1;
      // 
      // tb_translate_x
      // 
      this.tb_translate_x.Location = new System.Drawing.Point(7, 20);
      this.tb_translate_x.Name = "tb_translate_x";
      this.tb_translate_x.Size = new System.Drawing.Size(65, 20);
      this.tb_translate_x.TabIndex = 0;
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.tb_rotate_angle);
      this.groupBox1.Controls.Add(this.label1);
      this.groupBox1.Controls.Add(this.btn_rotate);
      this.groupBox1.Location = new System.Drawing.Point(16, 128);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(153, 79);
      this.groupBox1.TabIndex = 7;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Rotate";
      // 
      // tb_rotate_angle
      // 
      this.tb_rotate_angle.Location = new System.Drawing.Point(49, 19);
      this.tb_rotate_angle.Name = "tb_rotate_angle";
      this.tb_rotate_angle.Size = new System.Drawing.Size(98, 20);
      this.tb_rotate_angle.TabIndex = 7;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(9, 22);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(34, 13);
      this.label1.TabIndex = 6;
      this.label1.Text = "Angle";
      // 
      // gb_scale
      // 
      this.gb_scale.Controls.Add(this.tb_scale_y);
      this.gb_scale.Controls.Add(this.tb_scale_x);
      this.gb_scale.Controls.Add(this.btn_scale);
      this.gb_scale.Location = new System.Drawing.Point(16, 213);
      this.gb_scale.Name = "gb_scale";
      this.gb_scale.Size = new System.Drawing.Size(153, 78);
      this.gb_scale.TabIndex = 7;
      this.gb_scale.TabStop = false;
      this.gb_scale.Text = "Scale";
      // 
      // tb_scale_y
      // 
      this.tb_scale_y.Location = new System.Drawing.Point(78, 20);
      this.tb_scale_y.Name = "tb_scale_y";
      this.tb_scale_y.Size = new System.Drawing.Size(69, 20);
      this.tb_scale_y.TabIndex = 1;
      // 
      // tb_scale_x
      // 
      this.tb_scale_x.Location = new System.Drawing.Point(7, 20);
      this.tb_scale_x.Name = "tb_scale_x";
      this.tb_scale_x.Size = new System.Drawing.Size(65, 20);
      this.tb_scale_x.TabIndex = 0;
      // 
      // btn_scale
      // 
      this.btn_scale.Location = new System.Drawing.Point(7, 46);
      this.btn_scale.Name = "btn_scale";
      this.btn_scale.Size = new System.Drawing.Size(140, 23);
      this.btn_scale.TabIndex = 4;
      this.btn_scale.Text = "Scale";
      this.btn_scale.UseVisualStyleBackColor = true;
      this.btn_scale.Click += new System.EventHandler(this.btn_scale_Click);
      // 
      // gb_interpolation
      // 
      this.gb_interpolation.Controls.Add(this.rb_blinear);
      this.gb_interpolation.Controls.Add(this.rb_none);
      this.gb_interpolation.Location = new System.Drawing.Point(16, 298);
      this.gb_interpolation.Name = "gb_interpolation";
      this.gb_interpolation.Size = new System.Drawing.Size(153, 69);
      this.gb_interpolation.TabIndex = 8;
      this.gb_interpolation.TabStop = false;
      this.gb_interpolation.Text = "Interpolation";
      // 
      // rb_none
      // 
      this.rb_none.AutoSize = true;
      this.rb_none.Checked = true;
      this.rb_none.Location = new System.Drawing.Point(7, 20);
      this.rb_none.Name = "rb_none";
      this.rb_none.Size = new System.Drawing.Size(51, 17);
      this.rb_none.TabIndex = 0;
      this.rb_none.TabStop = true;
      this.rb_none.Text = "None";
      this.rb_none.UseVisualStyleBackColor = true;
      // 
      // rb_blinear
      // 
      this.rb_blinear.AutoSize = true;
      this.rb_blinear.Location = new System.Drawing.Point(7, 43);
      this.rb_blinear.Name = "rb_blinear";
      this.rb_blinear.Size = new System.Drawing.Size(60, 17);
      this.rb_blinear.TabIndex = 1;
      this.rb_blinear.Text = "bLinear";
      this.rb_blinear.UseVisualStyleBackColor = true;
      // 
      // bt_apply
      // 
      this.bt_apply.Location = new System.Drawing.Point(72, 122);
      this.bt_apply.Name = "bt_apply";
      this.bt_apply.Size = new System.Drawing.Size(75, 23);
      this.bt_apply.TabIndex = 9;
      this.bt_apply.Text = "Apply";
      this.bt_apply.UseVisualStyleBackColor = true;
      this.bt_apply.Click += new System.EventHandler(this.bt_apply_Click);
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.tableLayoutPanel1);
      this.groupBox2.Controls.Add(this.bt_apply);
      this.groupBox2.Location = new System.Drawing.Point(16, 373);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(153, 151);
      this.groupBox2.TabIndex = 10;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "groupBox2";
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(808, 546);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.gb_interpolation);
      this.Controls.Add(this.gb_scale);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.gb_translate);
      this.Controls.Add(this.btn_negative);
      this.Controls.Add(this.PictureBox);
      this.Controls.Add(this.btn_selectImage);
      this.Name = "Form1";
      this.Text = "Form1";
      ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).EndInit();
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      this.gb_translate.ResumeLayout(false);
      this.gb_translate.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.gb_scale.ResumeLayout(false);
      this.gb_scale.PerformLayout();
      this.gb_interpolation.ResumeLayout(false);
      this.gb_interpolation.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btn_selectImage;
    private System.Windows.Forms.OpenFileDialog FileDialog;
    private System.Windows.Forms.PictureBox PictureBox;
    private System.Windows.Forms.Button btn_negative;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.TextBox tm11;
    private System.Windows.Forms.TextBox tm33;
    private System.Windows.Forms.TextBox tm32;
    private System.Windows.Forms.TextBox tm31;
    private System.Windows.Forms.TextBox tm23;
    private System.Windows.Forms.TextBox tm22;
    private System.Windows.Forms.TextBox tm21;
    private System.Windows.Forms.TextBox tm13;
    private System.Windows.Forms.TextBox tm12;
    private System.Windows.Forms.Button bt_translate;
    private System.Windows.Forms.Button btn_rotate;
    private System.Windows.Forms.GroupBox gb_translate;
    private System.Windows.Forms.TextBox tb_translate_y;
    private System.Windows.Forms.TextBox tb_translate_x;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.TextBox tb_rotate_angle;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.GroupBox gb_scale;
    private System.Windows.Forms.TextBox tb_scale_y;
    private System.Windows.Forms.TextBox tb_scale_x;
    private System.Windows.Forms.Button btn_scale;
    private System.Windows.Forms.GroupBox gb_interpolation;
    private System.Windows.Forms.RadioButton rb_blinear;
    private System.Windows.Forms.RadioButton rb_none;
    private System.Windows.Forms.Button bt_apply;
    private System.Windows.Forms.GroupBox groupBox2;
  }
}

